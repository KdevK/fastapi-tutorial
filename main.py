from fastapi import FastAPI, Request
from starlette.templating import Jinja2Templates
from models import Car


app = FastAPI()

templates = Jinja2Templates(directory="templates")


@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get("/items/special")
def special_item():
    return {"item_id": 777, "description": "You have unlocked the special item"}


@app.get("/items/{item_id}")
def read_item(item_id: int, q: str | None = None):
    return {"item_id": item_id, "q": q}


@app.get("/advanced_items/{item_id}")
def read_user_item(
    item_id: str, name: str, quantity: int = 0, limit: int | None = None
):
    item = {"item_id": item_id, "name": name, "quantity": quantity, "limit": limit}
    return item


@app.get("/template/{name}")
def render_template(request: Request, name: str | None = None):
    return templates.TemplateResponse("index.html", {"request": request, "name": name})


@app.post("/car")
def process_car(car: Car):
    car_dict = car.dict()
    if car.tax:
        price_with_tax = car.price + car.tax
        car_dict.update({"price_with_tax": price_with_tax})
    return car_dict
