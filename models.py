from pydantic import BaseModel


class Car(BaseModel):
    name: str
    description: str | None = None
    price: float
    tax: float | None = None